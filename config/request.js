
const API = require('./api.js')

/**
 * [无图标，轻提示]
 * @param {[String]} title    [提示文字]
 * @param {[Int]}    duration [持续时间]
 */
const ToastNone = function (title, duration = 5000) {
  wx.showToast({ title, icon: 'none', duration })
}

/**
 * [发送请求 && 回调处理]
 * @param {[Object]}   that   [page对象]
 * @param {[String]}   path   [接口地址]
 * @param {[Object]}   data   [附带参数]
 * @param {[String]}   method [请求方式]
 * @param {[Function]} code_0 [返回成功时的处理方法]
 * @param {[Function]} code_1 [返回错误时的处理方法]
 * @param {[Function]} suc    [请求成功时的处理方法]
 * @param {[Function]} err    [请求失败时的处理方法]
 */
const Request = function (params) {
  const { path, data, method, code_0, code_1, suc, err } = params
  wx.request({
    url: `${API.host}${path}`, // 接口地址
    data,                      // 附带参数
    method,                    // 请求方式
    success(res) {
      if ('200' == res.data.code) {
        // 返回成功
        code_0(typeof res.data.data === 'string' ? JSON.parse(res.data.data) : res.data.data)
      } else {
        console.log(res.data)
        // 返回错误
        code_1(res)
      }
      // 请求成功
      suc(res)
    },
    fail(res) {
      console.log(res.data)
      // ToastNone('请求超时，请核实后重试！')
      // 请求失败
      err(res)
    }
  })
}

/************************************** 对外暴露 *************************************/

/**
 * [接收数据 && 请求配置]
 * @param {[String]}   name   [接口地址名称]
 * @param {[Object]}   obj    [请求附带数据]
 * @param {[String]}   way    [请求方式]
 * @param {[Function]} code_0 [返回成功时的处理方法]
 * @param {[Function]} code_1 [返回错误时的处理方法]
 * @param {[Function]} suc    [请求成功时的处理方法]
 * @param {[Function]} err    [请求失败时的处理方法]
 */
const request = function (params) {
  const {
    name,
    obj = {},
    way = '',
    code_0 = function () {},
    code_1 = function () {},
    suc = function () {},
    err = function () {}
  } = params
  // 接口地址
  const path = API.url[name]
  path ? '' : ToastNone(`/config/api.js 中没有配置 '${name}' 的地址！`)
  // 请求方式
  const method = typeof (way) === 'string' ? way || 'POST' : 'POST'
  // 合并参数
  let data = {}
  data = Object.assign(data, obj)

  /*** 发送请求 && 回调处理 ***/
  Request({ path, data, method, code_0, code_1, suc, err })
}

module.exports = request
