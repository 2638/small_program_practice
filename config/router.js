
const Path = {
  '列表': '/pages/list/list',
  '详情': '/pages/detail/detail'
}

/**
 * 路由跳转
 */
const route = {}

/**
 * [一般 跳转]
 * @param {[String]} name  路由名字
 * @param {[String]} param 附加参数
 */
route.goTo = function (name, param, success = function () {}) {
  wx.navigateTo({ url: `${Path[name]}${param}`, success })
}

/**
 * [关闭当前页面,返回之前页面]
 * @param {[Int]} delta 跳回去几页
 */
route.goBack = function (delta) {
  wx.navigateBack({ delta })
}

/**
 * [关闭当前页面 跳转]
 * @param {[String]} name  路由名字
 * @param {[String]} param 附加参数
 */
route.close = function (name, param, success = function () {}) {
  wx.redirectTo({ url: `${Path[name]}${param}`, success })
}

/**
 * [关闭所有页面，跳到某个页面]
 * @param {[String]} name  路由名字
 * @param {[String]} param 附加参数
 */
route.closeAll = function (name, param, success = function () {}) {
  wx.reLaunch({ url: `${Path[name]}${param}`, success })
}

/**
 * [跳到 tab 页]
 * @param {[String]} name  路由名字
 */
route.tab = function (name, success = function () {}) {
  wx.switchTab({ url: Path[name], success })
}



module.exports = route
