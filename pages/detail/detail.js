
const App = getApp()
const Other = require('./other.js')

Page({
  data: {
    title: '', // 顶部：标题
    text: '',  // 顶部：文字
    list: []   // 列表
  },

  toggle(e) {
    const that = this
    const key = e.currentTarget.dataset.key
    const { list } = that.data
    list[key].state = !list[key].state
    that.setData({ list })
  },

  /**************************** 生命周期 *****************************/

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const id = options.id || '11'
    console.log(id)
    /*** 请求详情 ***/
    Other.getDetail(this, id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
