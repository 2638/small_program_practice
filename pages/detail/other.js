

const App = getApp()

const obj = {}

/**
 * [请求详情]
 */
obj.getDetail = function (that, id) {
  const Other = this
  const name = '请求详情'
  const obj = { id }
  const err = function () {
    const ids = id.split('')
    const title = `第${ids[0]}次请求，第${ids[1]}条`
    const text = '随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点随便写点'
    that.setData({ title, text })
    /*** 删除空数据 ***/
    Other.delNull(that)
  }
  App.API({ name, obj, err })
}

/**
 * [删除空数据]
 */
obj.delNull = function (that) {
  const list = []
  for (let i = 0; i < 10; i++) {
    const obj = {
      id: i + 1,
      title: `第${i + 1}条`,
      content: `第${i + 1}条 -- 第${i + 1}条 -- 第${i + 1}条 -- 第${i + 1}条 -- 第${i + 1}条 -- 第${i + 1}条`,
      state: true
    }
    list.push(obj)
  }
  that.setData({ list })
}

module.exports = obj
