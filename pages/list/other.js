
const App = getApp()

const obj = {}

/**
 * [请求列表]
 */
obj.getList = function (that) {
  const Other = this
  const { page, isMore } = that.data
  if (isMore && page.num <=5) {
    that.setData ({ isMore: false })
    const { num, size, text } = page
    const name = '请求列表'
    const obj = { num, size }
    if (text) {
      obj.text = text
    }
    const err = function () {
      const arr = that.data.getList
      for (let i = 0; i < 5; i++) {
        const li = {
          id: `${num}${i + 1}`,
          img: `../../../images/components/listImg${i + 1}.png`,
          text: `第${num}次请求，第${i + 1}条`
        }
        arr.push(li)
      }
      page.num++
      that.setData({ getList: arr, isMore: true, page })
      /*** 筛选数组 ***/
      Other.indexOf(that)
    }
    App.API({ name, obj, err })
  } else if (isMore && page.num > 5) {
    /* 最多请求 5 次 */
    that.setData({ more: false })
  } else {
    /* 点的太快了 */
    wx.showToast({ title: '慢点', icon: 'none', duration: 1000 })
  }
}

/**
 * [筛选数组]
 */
obj.indexOf = function (that) {
  const { getList: arr, page: { text } } = that.data
  let list = []
  if (text) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].text.indexOf(text) !== -1) {
        list.push(arr[i])
      }
    }
  } else {
    list = arr
  }
  that.setData({ list })
}

module.exports = obj
