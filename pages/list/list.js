
const App = getApp()
const Other = require('./other.js')

Page({
  data: {
    page: {
      text: '', // 搜索关键字
      num: 1,   // 该请求第几页
      size: 5   // 每次请求几条数据
    },
    getList: [], // 请求到的原始数组
    list: [],    // 筛选后，显示的数组
    more: true,  // true：获取更多，false：已全部加载

    isMore: true // true：能点获取更多，false：不能点
  },

  /**************************** 页面事件 *****************************/

  /*** 获取更多 ***/
  more() {
    const that = this
    /*** 请求列表 ***/
    Other.getList(that)
  },

  /**************************** 组件事件 *****************************/

  /**
   * [搜索框：输入]
   */
  searchInput(e) {
    const that = this
    const { page } = that.data
    page.text = e.detail.text
    that.setData({ page })
    /*** 筛选数组 ***/
    Other.indexOf(that)
  },
  /**
   * [搜索框：点击搜索]
   */
  searchButton() {
    const that = this
    const { page } = that.data
    page.num = 1
    this.setData({
      page: page,
      getList: [],
      list: [],
      more: true,
      isMore: true
    })
    /*** 请求列表 ***/
    Other.getList(that)
  },

  /**
   * [跳转：详情]
   */
  goTo(e) {
    const name = '详情'
    const param = `?id=${e.detail.id}`
    App.Router.goTo(name, param)
  },

  /**************************** 生命周期 *****************************/

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
    /*** 请求列表 ***/
    Other.getList(that)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
